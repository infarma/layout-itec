## Arquivo de Pedido
gerador-layouts arquivoDePedido header NumeroSequencial:string:0:7 TipoRegistro:string:7:9 CodigoCliente:string:9:23 DataGeracao:string:23:31 HoraGeracao:string:31:35 NrPedido:string:35:49 PedidoConsulta:string:49:50 FormaPagamento:string:50:59 CodigoInternoFilial:string:59:64

gerador-layouts arquivoDePedido produto NumeroSequencial:string:0:7 TipoRegistro:string:7:9 CodigoProduto:string:9:22 Fixo:string:22:35 Quantidade:string:35:40 Filler:string:40:44

gerador-layouts arquivoDePedido trailler NumeroSequencial:string:0:7 TipoRegistro:string:7:9 QuantidadeRegistro:string:9:21 Filler:string:21:25


## Retorno de Pedido
gerador-layouts retornoDePedido header NumeroSequencial:string:0:7 TipoRegistro:string:7:9 CodigoCliente:string:9:23 DataGeracao:string:23:31 HoraGeracao:string:31:35 NrPedido:string:35:49 PedidoConsulta:string:49:50 NrPedidoFornecedor:string:50:64

gerador-layouts retornoDePedido produto NumeroSequencial:string:0:7 TipoRegistro:string:7:9 CodigoProduto:string:9:22 ValorUnitario:string:22:35 Quantidade:string:35:39 PercentualDesconto:string:39:48 CodigoMensagemRetorno:string:48:54

gerador-layouts retornoDePedido trailler NumeroSequencial:string:0:7 TipoRegistro:string:7:9 Fixo:string:9:19 ValorBruto:string:19:29 ValorDesconto:string:29:39 Filler:string:39:43
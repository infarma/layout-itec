package retornoDePedido

import (
	"fmt"
	"layout-itec/retornoDePedido"
	"testing"
	"time"
)

func TestHeaderRetorno (t *testing.T) {

	data := time.Date(2019, 10,1,1,5,20,0, time.UTC)

	cabecalho := retornoDePedido.GetHeaderRetorno(1111111, "33333333333333",data,data,66666666666666,7,88888888888888 )

	if cabecalho != "1111111013333333333333301102019010566666666666666788888888888888" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")

	}
}

func TestProdutoRetorno (t *testing.T) {

	// exemplo: 11111110233333333333334444444444.445555666666.66777777
	Valor9 := float64(666666.66)
	Valor13 := float64(4444444444.44)

	Produto := retornoDePedido.GetProdutoRetorno(1111111,"3333333333333", Valor13, 5555, Valor9, 777777)

	if Produto != "11111110233333333333334444444444.445555666666.66777777" {
		fmt.Println("Produto", Produto)
		t.Error("Produto não é compativel")

	}

}

func TestTrailerRetorno (t *testing.T) {

	// exemplo: 11111110300000000004444444.445555555.550000
	Valor4 := float64(4444444.44)
	Valor5 := float64(5555555.55)
	Trailer := retornoDePedido.GetTrailerRetorno(1111111,Valor4,Valor5)

	if Trailer != "11111110300000000004444444.445555555.550000" {
		fmt.Println("Trailer", Trailer)
		t.Error("Trailer não é compativel")

	}

}

package arquivoDePedidoTest

import (
	"layout-itec/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {

	f, err := os.Open("../fileTest/mock.txt")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var header arquivoDePedido.Header
	//1111111013333333333333344444444555566666666666666788888888899999
	header.NumeroSequencial = 1111111
	header.TipoRegistro = "01"
	header.CodigoCliente = "33333333333333"
	header.DataGeracao = 44444444
	header.HoraGeracao = 5555
	header.NrPedido = 66666666666666
	header.PedidoConsulta = 7
	header.FormaPagamento = 888888888
	header.CodigoInternoFilial = 99999

	if header.NumeroSequencial != arq.Header.NumeroSequencial {
		t.Error("NumeroSequencial não é compativel")
	}
	if header.TipoRegistro != arq.Header.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if header.CodigoCliente != arq.Header.CodigoCliente {
		t.Error("CodigoCliente não é compativel")
	}
	if header.DataGeracao != arq.Header.DataGeracao {
		t.Error("DataGeracao não é compativel")
	}
	if header.HoraGeracao != arq.Header.HoraGeracao {
		t.Error("HoraGeracao não é compativel")
	}
	if header.NrPedido != arq.Header.NrPedido {
		t.Error("NrPedido não é compativel")
	}
	if header.PedidoConsulta != arq.Header.PedidoConsulta {
		t.Error("PedidoConsulta não é compativel")
	}
	if header.FormaPagamento != arq.Header.FormaPagamento {
		t.Error("FormaPagamento não é compativel")
	}
	if header.CodigoInternoFilial != arq.Header.CodigoInternoFilial {
		t.Error("CodigoInternoFilial não é compativel")
	}

	var Produto arquivoDePedido.Produto
	//11111110233333333333334444444444444555556666
	Produto.NumeroSequencial = 1111111
	Produto.TipoRegistro  = "02"
	Produto.CodigoProduto = 3333333333333
	Produto.Fixo = 4444444444444
	Produto.Quantidade = 55555
	Produto.Filler =  6666

	if Produto.NumeroSequencial != arq.Produto[0].NumeroSequencial {
		t.Error("NumeroSequencial não é compativel")
	}
	if Produto.TipoRegistro != arq.Produto[0].TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Produto.CodigoProduto != arq.Produto[0].CodigoProduto {
		t.Error("CodigoProduto não é compativel")
	}
	if Produto.Fixo != arq.Produto[0].Fixo {
		t.Error("Fixo não é compativel")
	}
	if Produto.Quantidade != arq.Produto[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}
	if Produto.Filler != arq.Produto[0].Filler {
		t.Error("Filler não é compativel")
	}

	var Trailer arquivoDePedido.Trailler
	//1111111033333333333334444

	Trailer.NumeroSequencial = 1111111
	Trailer.TipoRegistro = "03"
	Trailer.QuantidadeRegistro = 333333333333
	Trailer.Filler  = 4444


	if Trailer.NumeroSequencial != arq.Trailler.NumeroSequencial {
		t.Error("NumeroSequencial não é compativel")
	}
	if Trailer.TipoRegistro != arq.Trailler.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Trailer.QuantidadeRegistro != arq.Trailler.QuantidadeRegistro {
		t.Error("QuantidadeRegistro não é compativel")
	}
	if Trailer.Filler != arq.Trailler.Filler {
		t.Error("Filler não é compativel")
	}
}

package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	NumeroSequencial    int64	`json:"NumeroSequencial"`
	TipoRegistro        string	`json:"TipoRegistro"`
	CodigoCliente       string	`json:"CodigoCliente"`
	DataGeracao         int64	`json:"DataGeracao"`
	HoraGeracao         int64	`json:"HoraGeracao"`
	NrPedido            int64	`json:"NrPedido"`
	PedidoConsulta      int64	`json:"PedidoConsulta"`
	FormaPagamento      int64	`json:"FormaPagamento"`
	CodigoInternoFilial int64	`json:"CodigoInternoFilial"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataGeracao, "DataGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.HoraGeracao, "HoraGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NrPedido, "NrPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.PedidoConsulta, "PedidoConsulta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.FormaPagamento, "FormaPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoInternoFilial, "CodigoInternoFilial")
	if err != nil {
		return err
	}


	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":                      {0, 7, 0},
	"TipoRegistro":                      {7, 9, 0},
	"CodigoCliente":                      {9, 23, 0},
	"DataGeracao":                      {23, 31, 0},
	"HoraGeracao":                      {31, 35, 0},
	"NrPedido":                      {35, 49, 0},
	"PedidoConsulta":                      {49, 50, 0},
	"FormaPagamento":                      {50, 59, 0},
	"CodigoInternoFilial":                      {59, 64, 0},
}
package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produto struct {
	NumeroSequencial      string	`json:"NumeroSequencial"`
	TipoRegistro          string	`json:"TipoRegistro"`
	CodigoProduto         string	`json:"CodigoProduto"`
	ValorUnitario         string	`json:"ValorUnitario"`
	Quantidade            string	`json:"Quantidade"`
	PercentualDesconto    string	`json:"PercentualDesconto"`
	CodigoMensagemRetorno string	`json:"CodigoMensagemRetorno"`
}

func (p *Produto) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProduto

	err = posicaoParaValor.ReturnByType(&p.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorUnitario, "ValorUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PercentualDesconto, "PercentualDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoMensagemRetorno, "CodigoMensagemRetorno")
	if err != nil {
		return err
	}


	return err
}

var PosicoesProduto = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":                      {0, 7, 0},
	"TipoRegistro":                      {7, 9, 0},
	"CodigoProduto":                      {9, 22, 0},
	"ValorUnitario":                      {22, 35, 0},
	"Quantidade":                      {35, 39, 0},
	"PercentualDesconto":                      {39, 48, 0},
	"CodigoMensagemRetorno":                      {48, 54, 0},
}
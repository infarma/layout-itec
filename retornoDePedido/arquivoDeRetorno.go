package retornoDePedido

import (
	"fmt"
	"math/big"
	"time"
)

func GetHeaderRetorno(	NumeroSequencial int64,  CodigoCliente string, DataGeracao time.Time, HoraGeracao time.Time, NrPedido int64, PedidoConsulta int32, NrPedidoFornecedor int64 ) string{

	// exemplo : 1111111013333333333333301102019010566666666666666788888888888888
	cabecalho := fmt.Sprintf("%07d", NumeroSequencial)
	//tipo de registro
	cabecalho += "01"
	cabecalho += fmt.Sprintf("%014s", CodigoCliente)
	cabecalho += DataGeracao.Format("02012006")
	cabecalho += HoraGeracao.Format("1504")
	cabecalho += fmt.Sprintf("%014d", NrPedido )
	cabecalho += fmt.Sprintf("%01d", PedidoConsulta )
	cabecalho += fmt.Sprintf("%014d", NrPedidoFornecedor )

	return cabecalho
}

func GetProdutoRetorno(	NumeroSequencial int64,  CodigoProduto string, ValorUnitario float64, Quantidade int32, PercentualDesconto float64, CodigoMensagemRetorno int64) string{

	// exemplo: 11111110233333333333334444444444.445555666666.66777777
	Produto := fmt.Sprintf("%07d", NumeroSequencial)
	//tipo de registro
	Produto += "02"
	Produto += fmt.Sprintf("%013s", CodigoProduto)
	Produto += fmt.Sprintf("%013s", big.NewFloat(ValorUnitario).SetMode(big.AwayFromZero).Text('f', 2))
	Produto += fmt.Sprintf("%04d", Quantidade )
	Produto += fmt.Sprintf("%09s", big.NewFloat(PercentualDesconto).SetMode(big.AwayFromZero).Text('f', 2))
	Produto += fmt.Sprintf("%06d", CodigoMensagemRetorno)

	return Produto
}

func GetTrailerRetorno(	NumeroSequencial int64, ValorBruto float64, ValorDesconto float64 ) string{

	// exemplo: 111111103000000004444444.445555555.550000
	Trailer := fmt.Sprintf("%07d", NumeroSequencial)
	//tipo de registro
	Trailer += "03"
	//fixo
	Trailer += fmt.Sprintf("%010s", "")
	Trailer += fmt.Sprintf("%010s", big.NewFloat(ValorBruto).SetMode(big.AwayFromZero).Text('f', 2))
	Trailer += fmt.Sprintf("%010s", big.NewFloat(ValorDesconto).SetMode(big.AwayFromZero).Text('f', 2))
	//filler
	Trailer += fmt.Sprintf("%04s", "")

	return Trailer
}
